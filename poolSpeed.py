from numpy import matmul, dot, array
from numpy.random import rand
from multiprocessing.dummy import Pool

from time import clock
from sys import argv

def mult(ab) :
    return dot(ab[0],ab[1])

if __name__=="__main__" :
    # N is the size of the square matrix, K is the number of them
    N = int(argv.pop(1))
    K = int(argv.pop(1))
    alpha = Pool(K)
    A = [rand(N,N) for k in range(K)]
    B = [rand(N,N) for k in range(K)]
    
    startTime = clock()
    C = alpha.map(mult, zip(A,B))
    #alpha.join()
    #alpha.close()
    endTime = clock()
    
    print(endTime - startTime)
