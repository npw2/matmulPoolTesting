from numpy import matmul, dot
from numpy.random import rand
from multiprocessing.dummy import Pool

from time import clock
from sys import argv

if __name__=="__main__" :
    # N is the size of the square matrix, K is the number of them
    N = int(argv.pop(1))
    K = int(argv.pop(1))
    #alpha = Pool(K)
    A = rand(K,N,N)
    B = rand(K,N,N)
    
    startTime = clock()
    C = matmul(A,B)
    endTime = clock()
    print(endTime - startTime)

