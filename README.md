Testing the efficiency of matmul vs multiprocessing.dummy (ie threading). Should also test difference between multiprocessing and matmul.

Current results indicate that matmul is not only faster, but also more stable.